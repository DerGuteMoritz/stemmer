(use stemmer test)

(test-assert (member 'english (available-stemmers)))

(define english (make-stemmer 'english))

(test "build" (stem english "buildings"))
(test "build" (stem english "builds"))
(test "Häuser" (stem english "Häuser"))

(define german (make-stemmer 'german))
(test "Haus" (stem german "Häuser"))